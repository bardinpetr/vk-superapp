package com.minutetodeadline.vk.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Community(val id: Long, val name: String, val photo: String) : Parcelable