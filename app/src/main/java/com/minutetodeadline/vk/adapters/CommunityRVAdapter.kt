package com.minutetodeadline.vk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter
import com.minutetodeadline.vk.R
import com.minutetodeadline.vk.models.Community
import jp.wasabeef.glide.transformations.CropCircleWithBorderTransformation

class CommunityRVAdapter(var ctx: Context?, var communities: List<Community>) :
    MultiChoiceAdapter<CommunityRVAdapter.ViewHolder>() {
    private var inflater: LayoutInflater = LayoutInflater.from(ctx)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.card_community, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setText(communities[position].name)
        holder.setImage(communities[position].photo, selectedItemList.contains(position))

        super.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int = communities.size

    override fun setActive(view: View, state: Boolean) {}

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameText = itemView.findViewById<TextView>(R.id.card_community_name)
        private val imageView = itemView.findViewById<ImageView>(R.id.card_community_image)

        fun setImage(url: String, bordered: Boolean) {
            var base = Glide
                .with(itemView)
                .load(url)
                .override(200, 200)

            base = if (bordered)
                base.apply(
                    RequestOptions.bitmapTransform(
                        CropCircleWithBorderTransformation(
                            itemView.context.resources
                                .getDimension(R.dimen.img_corner_radius)
                                .toInt(),
                            itemView.context.resources
                                .getColor(R.color.vk_color, itemView.context.theme)
                        )
                    )
                )
            else
                base.circleCrop()

            base.into(imageView)
        }

        fun setText(text: String) {
            nameText.text = text
        }
    }
}