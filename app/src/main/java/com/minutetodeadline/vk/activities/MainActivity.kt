package com.minutetodeadline.vk.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.minutetodeadline.vk.R
import com.minutetodeadline.vk.adapters.ViewPagerAdapter
import com.minutetodeadline.vk.databinding.ActivityMainBinding
import com.minutetodeadline.vk.fragments.CommunitiesFragment
import com.minutetodeadline.vk.fragments.UserFragment


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var prevMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewPager()
    }

    private fun initViewPager() {
        val navigationMap = mapOf(
            R.id.navigation_user to 0,
            R.id.navigation_communities to 1
        )

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(UserFragment())
        adapter.addFragment(CommunitiesFragment())
        binding.viewpagerMain.adapter = adapter

        binding.navView.setOnItemSelectedListener { item ->
            binding.viewpagerMain.currentItem = navigationMap[item.itemId] ?: R.string.me
            false
        }

        binding.viewpagerMain.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (prevMenuItem != null)
                    prevMenuItem!!.isChecked = false
                else
                    binding.navView.menu.getItem(0).isChecked = false
                binding.navView.menu.getItem(position).isChecked = true
                prevMenuItem = binding.navView.menu.getItem(position)
                supportActionBar!!.title = prevMenuItem!!.title
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        supportActionBar!!.title = binding.navView.menu.getItem(0)!!.title
    }
}