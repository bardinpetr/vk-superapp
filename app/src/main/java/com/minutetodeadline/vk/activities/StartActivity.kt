package com.minutetodeadline.vk.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.minutetodeadline.vk.R
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import com.vk.api.sdk.exceptions.VKAuthException


class StartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        if (VK.isLoggedIn()) {
            startMain()
            finish()
        } else {
            findViewById<Button>(R.id.button).setOnClickListener {
                VK.login(this, arrayListOf(VKScope.FRIENDS, VKScope.PHOTOS))
            }
        }
    }

    private fun startMain() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                startMain()
            }

            override fun onLoginFailed(authException: VKAuthException) {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.try_again),
                    Toast.LENGTH_LONG
                ).show();
            }
        }
        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

}