package com.minutetodeadline.vk.fragments

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import app.juky.squircleview.views.SquircleButton
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.transition.Transition
import com.minutetodeadline.vk.adapters.FriendRVAdapter
import com.minutetodeadline.vk.databinding.FragmentUserBinding
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import com.vk.dto.common.id.UserId
import com.vk.sdk.api.account.AccountService
import com.vk.sdk.api.account.dto.AccountUserSettings
import com.vk.sdk.api.friends.FriendsService
import com.vk.sdk.api.friends.dto.FriendsGetFieldsResponse
import com.vk.sdk.api.photos.PhotosService
import com.vk.sdk.api.photos.dto.PhotosGetResponse
import com.vk.sdk.api.photos.dto.PhotosPhotoSizesType
import com.vk.sdk.api.users.dto.UsersFields

class UserFragment : Fragment() {
    private var friendsList: ArrayList<String> = arrayListOf()

    private var _binding: FragmentUserBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserBinding.inflate(inflater, container, false)

        val adapter = FriendRVAdapter(context, friendsList)

        val layoutManager = LinearLayoutManager(context)
        binding.friendsRv.layoutManager = layoutManager
        binding.friendsRv.adapter = adapter

        getInfo()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getInfo() {
        val uid = UserId(VK.getUserId().toLong())

        VK.execute(
            AccountService().accountGetProfileInfo(),
            object : VKApiCallback<AccountUserSettings> {
                override fun fail(error: Exception) {}

                @SuppressLint("SetTextI18n")
                override fun success(result: AccountUserSettings) {
                    binding.userName.text = "${result.firstName} ${result.lastName}"
                }
            })

        VK.execute(
            PhotosService().photosGet(uid, "profile"),
            object : VKApiCallback<PhotosGetResponse> {
                override fun fail(error: Exception) {}

                @SuppressLint("SetTextI18n")
                override fun success(result: PhotosGetResponse) {
                    if (result.count > 0) {
                        result.items[result.count - 1].sizes?.forEach { i ->
                            if (i.type == PhotosPhotoSizesType.M)
                                Glide
                                    .with(context!!)
                                    .load(i.url)
                                    .into(object :
                                        CustomViewTarget<SquircleButton, Drawable>(binding.userPhoto) {
                                        override fun onResourceReady(
                                            resource: Drawable,
                                            transition: Transition<in Drawable>?
                                        ) {
                                            binding.userPhoto.style.setBackgroundImage(resource)
                                        }

                                        override fun onLoadFailed(errorDrawable: Drawable?) {}
                                        override fun onResourceCleared(placeholder: Drawable?) {}
                                    })
                        }
                    }
                }
            })

        VK.execute(
            FriendsService().friendsGet(
                uid,
                fields = listOf(UsersFields.FIRST_NAME_NOM, UsersFields.LAST_NAME_NOM)
            ),
            object : VKApiCallback<FriendsGetFieldsResponse> {
                override fun fail(error: Exception) {

                }

                @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
                override fun success(result: FriendsGetFieldsResponse) {
                    binding.friendsText.text = "Друзья (${result.count}):"
                    if (friendsList.size > 0) {
                        friendsList.clear()
                        binding.friendsRv.adapter!!.notifyDataSetChanged()
                    }
                    friendsList.addAll(result.items.map { i -> "${i.firstName} ${i.lastName}" })
                    binding.friendsRv.adapter!!.notifyItemRangeInserted(0, result.count)
                }
            })
    }
}