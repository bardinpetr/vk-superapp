package com.minutetodeadline.vk.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter
import com.minutetodeadline.vk.adapters.CommunityRVAdapter
import com.minutetodeadline.vk.databinding.FragmentCommunitiesBinding
import com.minutetodeadline.vk.models.Community
import com.minutetodeadline.vk.tools.VKTools.Companion.getGroups


class CommunitiesFragment : Fragment() {

    private var _binding: FragmentCommunitiesBinding? = null
    private val binding get() = _binding!!

    private var communities: ArrayList<Community> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCommunitiesBinding.inflate(inflater, container, false)

        initRecyclerView()

        getGroups({
            communities.clear()
            communities.addAll(it.map { i -> Community(i.id!!.value, i.name!!, i.photo200!!) })
            binding.communitiesRv.adapter!!.notifyDataSetChanged()
        })

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initRecyclerView() {
        binding.communitiesRv.layoutManager = GridLayoutManager(context, 3)

        val adapter = CommunityRVAdapter(context, communities)
        binding.communitiesRv.adapter = adapter

        adapter.setMultiChoiceSelectionListener(object : MultiChoiceAdapter.Listener {
            override fun OnItemSelected(
                selectedPosition: Int,
                itemSelectedCount: Int,
                allItemCount: Int
            ) {
            }

            override fun OnItemDeselected(
                deselectedPosition: Int,
                itemSelectedCount: Int,
                allItemCount: Int
            ) {
            }

            override fun OnSelectAll(itemSelectedCount: Int, allItemCount: Int) {}
            override fun OnDeselectAll(itemSelectedCount: Int, allItemCount: Int) {}
        })
    }
}