package com.minutetodeadline.vk

import android.app.Application
import android.widget.Toast
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKTokenExpiredHandler


class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        VK.addTokenExpiredHandler(
            object : VKTokenExpiredHandler {
                override fun onTokenExpired() {
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.session_ended),
                        Toast.LENGTH_LONG
                    ).show();
                }
            }
        )
    }

}
