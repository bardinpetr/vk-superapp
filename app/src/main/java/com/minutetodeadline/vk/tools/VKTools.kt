package com.minutetodeadline.vk.tools

import android.annotation.SuppressLint
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import com.vk.dto.common.id.UserId
import com.vk.sdk.api.groups.GroupsService
import com.vk.sdk.api.groups.dto.GroupsFields
import com.vk.sdk.api.groups.dto.GroupsFilter
import com.vk.sdk.api.groups.dto.GroupsGetObjectExtendedResponse
import com.vk.sdk.api.groups.dto.GroupsGroupFull

class VKTools {
    companion object {
        fun getGroups(callback: (List<GroupsGroupFull>) -> Unit, filterByAdmin: Boolean = false) {
            val filter = if (filterByAdmin) listOf(GroupsFilter.ADMIN) else null
            VK.execute(
                GroupsService().groupsGetExtended(
                    UserId(VK.getUserId().toLong()),
                    filter = filter
                ),
                object : VKApiCallback<GroupsGetObjectExtendedResponse> {
                    override fun fail(error: Exception) {}

                    @SuppressLint("SetTextI18n")
                    override fun success(result: GroupsGetObjectExtendedResponse) {
                        callback(result.items)
                    }
                })

        }
    }
}